#include <bits/stdc++.h>

using namespace std;

typedef string twochar;

void readRawData(string fileName, vector<string> &raw_data);
long generateSixDigit();
twochar mappingDigitToChar(int );
void getWordFromRawData(vector<string> &raw_data, vector<vector<string> > &words);
void getSentenceFromWords(vector<vector<string> > &sentence, vector<vector<string> > &words, string otp[6]);
int decodeWordToOtp(string );
int mappingOtpToDigit(twochar);
void printSentence(vector<string> );
void printLabel(vector<int> );
vector<int> getBinerLabel(const vector<string>& str, const string otp[6]);

int main() {
    int i, j;
    long n, n_val;

    vector<string> raw_data;
    vector<vector<string> > sentences;
    vector<vector<string> > words;
    twochar otp[6];

    readRawData("pos_tag_noun.txt", raw_data);
    
    getWordFromRawData(raw_data, words);
    while (sentences.empty()) {
        n = generateSixDigit();
        n_val = n;
        i = 5;
        while (i >= 0) {
            otp[i] = mappingDigitToChar(n % 10);
            n /= 10;
            i--;
        }
        getSentenceFromWords(sentences, words, otp);

    }
    cout << "OTP: " << n_val << endl;
    for (i = 0; i < 6; i++) {
        cout << "otp" << i + 1 << ": " << otp[i] << endl;
    }

    for (i = 0; i < sentences.size(); i++) {
        printSentence(sentences[i]);
        vector<int> label = getBinerLabel(sentences[i], otp);
        printLabel(label);
        for (j = sentences[i].size()-1; j >= 0; j--) {
            cout << decodeWordToOtp(sentences[i][j]);
        }
        cout << endl;
    }
    

    return 0;
}

vector<int> getBinerLabel(const vector<string>& str, const string otp[6]) {
    int i, j, idx_otp;
    vector<int> ret;

    idx_otp = 0;
    for (i = str.size()-1; i >= 0; i--) {
        ret.push_back(0);
        for (j = 0; j < str[i].length()-1; j++) {
            // cout << str[i][j] << str[i][j+1] << endl;
            if (idx_otp == 6) {
               ret.push_back(0);
            } else {
                if (str[i][j] == otp[idx_otp][0] && str[i][j + 1] == otp[idx_otp][1]) {
                    // cout << otp[idx_otp] << endl;
                    idx_otp++;
                    ret.push_back(1);
                } else {
                    ret.push_back(0);
                }
            }
        } 
        ret.push_back(0);   
    }

    return ret;
}

void printLabel(vector<int> label) {
    for (int j = 0; j < label.size(); j++){
        cout << label[j];
    }
    cout << endl;
}

void printSentence(vector<string> s) {
    int i;
    for (i = s.size() - 1; i >= 0; i--) {
        cout << s[i] << " ";
    }
    cout << endl;
}


int decodeWordToOtp(string word) {
    int i, result;

    result = -1;  

    for (i = 0; i < word.length()-1; i++) {
        int digit = mappingOtpToDigit(word.substr(i, 2)); 
        if ( digit != -1) {
            if (result == -1) {
                result = digit;
            } else {
                result *= 10;
                result += digit;
            }
        } 
    }

    return result;
}

void getSentenceFromWords(vector<vector<string> > &sentences, vector<vector<string> > &words, string otp[6]) {
    int i, j, idx, pos_first, pos_second, otp_value, temp;
    bool two_otp;
    vector<string> sentence;
    for (i = 0; i < words.size(); i++) {
        idx = 5;
        for (j = words[i].size()-1; j >= 0 && idx >= 0; j--) {
            two_otp = false;
            pos_first = words[i][j].rfind(otp[idx]);
            if (pos_first != string::npos) {
                otp_value = mappingOtpToDigit(otp[idx]);
                idx--;
                if (idx >= 0) {
                    pos_second = words[i][j].rfind(otp[idx]);
                    if (pos_second != string::npos) {
                        if (pos_second < pos_first) {
                            otp_value += 10*mappingOtpToDigit(otp[idx]);
                            if (otp_value == decodeWordToOtp(words[i][j])) {
                                sentence.push_back(words[i][j]);
                                // cout << "push back: " << idx << " ";
                                // cout << otp_value << " " << words[i][j] << endl;
                                idx--;
                                two_otp = true;
                            }
                        }
                    } 
                }
                if (!two_otp) {
                    if (otp_value == decodeWordToOtp(words[i][j])) {
                        sentence.push_back(words[i][j]);
                        // cout << "push back: " << idx << " ";
                        // cout << otp_value << " " << words[i][j] << endl;
                    }
                    else {
                        idx++;
                    }
                } 
            }
        }
        if (sentence.size() >= 3 && idx < 0) {
            sentences.push_back(sentence);
        }
        sentence.clear();
    }
}

void getWordFromRawData(vector<string> &raw_data, vector<vector<string> > &words) {
    int i, index;    
    size_t pos;

    // for (i = 0; i < 1; i++) {
    for (i = 0; i < raw_data.size(); i++) {
        vector<string> temp;
        char *pch;
        char *cstr = new char[(raw_data[i].length() + 1)];
        strcpy(cstr, raw_data[i].c_str()); 
        pch = strtok(cstr, " ,.-");
        while (pch != NULL) {
            // printf("%s\n", pch);
            string str(pch);
            temp.push_back(str);
            pch = strtok(NULL, " ,.-");
        }
        words.push_back(temp);
        temp.clear();
        delete[] cstr;      
        delete[] pch;
    }
}


//membaca data dari file eksternal
void readRawData(string fileName, vector<string> &raw_data) {
    fileName.c_str();

    // Pembacaan file eksternal
    std::ifstream infile(fileName.c_str());
    std::string line;
    //baca per line
    while (std::getline(infile, line)){
        raw_data.push_back(line);
    }
}



long generateSixDigit() {
    long i = 0;
    long range_min = 100000;
    long range_max = 999999;
    srand(time(NULL));
    while (i < 99999 || i > 999999) {
        i = (double)rand() / (RAND_MAX + 1) * (range_max - range_min) + range_min;
        cout << i << endl;
    }
    return i;
}

int mappingOtpToDigit(twochar str) {
    int digit = -1; 
    
    if (str == "ga")
        digit = 0;  
    if (str == "ta")
        digit = 1;  
    if (str == "at")
        digit = 2;  
    if (str == "ya")
        digit = 3;  
    if (str == "da")
        digit = 4;  
    if (str == "ka")
        digit = 5;  
    if (str == "ar")
        digit = 6;  
    if (str == "la")
        digit = 7;  
    if (str == "me")
        digit = 8;  
    if (str == "in")
        digit = 9; 

    return digit;
}

twochar mappingDigitToChar(int x) {
    string str;
    switch (x) {
        case 0:
            str = "ga";  
            break;
        case 1:
            str = "ta";  
            break;
        case 2:
            str = "at";  
            break;
        case 3:
            str = "ya";  
            break;
        case 4:
            str = "da";  
            break;
        case 5:
            str = "ka";  
            break;
        case 6:
            str = "ar";  
            break;
        case 7:
            str = "la";  
            break;
        case 8:
            str = "me";  
            break;
        case 9:
            str = "in";  
            break;
        default:
            str = "ga";
    }

    return str;
}
